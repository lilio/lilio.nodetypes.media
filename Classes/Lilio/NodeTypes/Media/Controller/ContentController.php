<?php

namespace Lilio\NodeTypes\Media\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "LiliO.NodeTypes.Media". *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU General Public License, either version 3 of the   *
 * License, or (at your option) any later version.                        *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;

/**
 *
 * @Flow\Scope("singleton")
 */
class ContentController extends \TYPO3\Flow\Mvc\Controller\ActionController {
	/**
	 * @var array
	 */
	protected $supportedMediaTypes = array('application/json');

	/**
	 * @var array
	 */
	protected $viewFormatToObjectNameMap = array('json' => 'TYPO3\Flow\Mvc\View\JsonView');


	/**
	 * @return string
	 */
	public function tagsAction() {

		/** @var @var $tagRepository \TYPO3\Media\Domain\Repository\TagRepository */
		$tagRepository = $this->objectManager->get('\\TYPO3\\Media\\Domain\\Repository\\TagRepository');
		/** @var $persistenceManager \TYPO3\Flow\Persistence\PersistenceManagerInterface */
		$persistenceManager = $this->objectManager->get('TYPO3\\Flow\\Persistence\\PersistenceManagerInterface');

		$tags = array();
		foreach($tagRepository->findAll() as $tag) {
			/** @var $tag \TYPO3\Media\Domain\Model\Tag */

			$item = array(
				'label' => $tag->getLabel()
			);

			$tags[$persistenceManager->getIdentifierByObject($tag)] = $item;
		}

		$this->view->assign('value', $tags);
	}

} 