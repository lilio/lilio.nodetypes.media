<?php

namespace Lilio\NodeTypes\Media\ViewHelpers;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "LiliO.NodeTypes.Media". *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU General Public License, either version 3 of the   *
 * License, or (at your option) any later version.                        *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\Flow\Annotations as Flow;

class AssetsForTagViewHelper extends AbstractViewHelper {

	/**
	 * @Flow\Inject
	 * @var \TYPO3\Media\Domain\Repository\AssetRepository
	 */
	protected $assetRespository;


	/**
	 * @Flow\Inject
	 * @var \TYPO3\Media\Domain\Repository\TagRepository
	 */
	protected $tagRepository;


	/**
	 * @param \TYPO3\Media\Domain\Model\Tag $tag
	 * @param string $tagIdentifier
	 * @return string
	 */
	public function render(\TYPO3\Media\Domain\Model\Tag $tag = NULL, $identifier = NULL) {

		if($tag === NULL && $identifier !== '') {
			$tag = $this->tagRepository->findByIdentifier($identifier);
		}

		if($tag) {
			return $this->assetRespository->findByTag($tag);
		} else {
			return NULL;
		}

	}

} 