<?php
namespace Lilio\NodeTypes\Media\NodeTypePostprocessor;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "LiliO.NodeTypes.Media". *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU General Public License, either version 3 of the   *
 * License, or (at your option) any later version.                        *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\TYPO3CR\Domain\Model\NodeType;
use TYPO3\TYPO3CR\NodeTypePostprocessor\NodeTypePostprocessorInterface;


/**
 * This Processor updates the List NodeType with the existing tags.
 */
class ListFilesNodeTypePostprocessor implements NodeTypePostprocessorInterface {

	/**
	 * @Flow\Inject
	 * @var \TYPO3\Flow\Persistence\PersistenceManagerInterface
	 */
	protected $persistenceManager;

	/**
	 * Returns the processed Configuration
	 *
	 * @param \TYPO3\TYPO3CR\Domain\Model\NodeType $nodeType (uninitialized) The node type to process
	 * @param array $configuration input configuration
	 * @param array $options The processor options
	 * @return void
	 */
	public function process(NodeType $nodeType, array &$configuration, array $options) {

		$tagRepository = new \TYPO3\Media\Domain\Repository\TagRepository();


		$tags = array();
		foreach($tagRepository->findAll() as $tag) {
			/** @var $tag \TYPO3\Media\Domain\Model\Tag */

			$item = array(
				'label' => $tag->getLabel()
			);

			$tags[$this->persistenceManager->getIdentifierByObject($tag)] = $item;
		}


		$configuration['properties']['tag']['ui']['inspector']['editorOptions']['values'] = array_merge(
			$configuration['properties']['tag']['ui']['inspector']['editorOptions']['values'],
			$tags
		);
	}
}
