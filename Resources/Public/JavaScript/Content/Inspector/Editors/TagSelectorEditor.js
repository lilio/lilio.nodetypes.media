/*                                                                        *
 * This script belongs to the TYPO3 Flow package "LiliO.NodeTypes.Media". *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU General Public License, either version 3 of the   *
 * License, or (at your option) any later version.                        *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

define(
	[
		'Library/jquery-with-dependencies',
		'Content/Inspector/Editors/SelectBoxEditor',
		'Content/Inspector/InspectorController'
	],
	function(
		$,
		SelectBoxEditor,
		InspectorController
		) {
		return SelectBoxEditor.extend({
			init: function() {
				var that = this;

				this.set('placeholder', 'Loading ...');
				this._loadValuesFromController('/neos/backend/lilionodetypesmedia/content/tags.json', function(results) {
					var values = {}, placeholder;

					placeholder = 'No tags available';
					if (results === null) {
						results = [];
					} else {
						if (results.length > 0) {
							placeholder = 'Select a tag';
						}
					}
					that.setProperties({
						placeholder: placeholder,
						values: results
					});
				});

				this._super();
			}
		});
	});
